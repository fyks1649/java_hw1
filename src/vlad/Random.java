package vlad;

import java.util.Arrays;
import java.util.Scanner;

public class Random {
    public static void main(String[] args) {
        int randomNumber = (int) (Math.random() * 100);
        int number;

        System.out.println("Let the game begin!");
        Scanner scanner = new Scanner(System.in);
        System.out.println("What is your name?");
        String name = scanner.nextLine();
        int[] arrayNumbers = {};

        do {
            System.out.print("Enter your number: ");
            while (!scanner.hasNextInt()) {
                System.out.println("Enter a number please!");
                scanner.next();
            }
            number = scanner.nextInt();

                if (number < randomNumber) {
                    arrayNumbers = addNumber(arrayNumbers, number);
                    System.out.println("Your number is too small. Please, try again!");

                } else if (number > randomNumber) {
                    arrayNumbers = addNumber(arrayNumbers, number);
                    System.out.println("Your number is too big. Please, try again!");

                } else {
                    arrayNumbers = addNumber(arrayNumbers, number);
                    Arrays.sort(arrayNumbers);
                    System.out.println("Congratulations, " + name + "!");
                    System.out.println("Your numbers: " + Arrays.toString(revert(arrayNumbers)).replaceAll("[\\[\\]]", ""));
                }

        } while (number != randomNumber);
    }

    private static int[] addNumber(int[] savedNumbers, int lastInputedNumber) {
        savedNumbers = Arrays.copyOf(savedNumbers, savedNumbers.length + 1);
        savedNumbers[savedNumbers.length - 1] = lastInputedNumber;
        return savedNumbers;
    }

    private static int[] revert(int[] value) {
        int temp;
        for (int i = 0; i < value.length / 2; i++) {
            temp = value[i];
            value[i] = value[value.length - i - 1];
            value[value.length - i - 1] = temp;
        }
        return value;
    }
}
